FROM golang

# Fetch dependencies
RUN go get github.com/tools/godep

# Add project directory to Docker image.
ADD . /go/src/bitbucket.org/stereosteve/partopus

ENV USER steve
ENV HTTP_ADDR 8888
ENV HTTP_DRAIN_INTERVAL 1s
ENV COOKIE_SECRET 9sYkOYy3U7qjVQg8

# Replace this with actual PostgreSQL DSN.
ENV DSN postgres://steve@localhost:5432/partopus?sslmode=disable

WORKDIR /go/src/bitbucket.org/stereosteve/partopus

RUN godep go build

EXPOSE 8888
CMD ./partopus